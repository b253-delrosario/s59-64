
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link, NavLink } from 'react-router-dom';

import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';


export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return (

		<Navbar id="navbar" expand="lg" /*className="fixed-top"*/>
			<Container >
				<Navbar.Brand id="nav-brand" as={ Link } to="/">CHIBABA.</Navbar.Brand>
				<Navbar.Toggle />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav id="nav-text" className="ms-auto">
						{ user.id === null ? (
						    <>
						    	<Nav.Link className="product" as={ NavLink } to="/products">PRODUCTS</Nav.Link>
						    	<Nav.Link className="login" as={ NavLink } to="/login">LOG IN</Nav.Link>
						    	<Nav.Link className="register" as={ NavLink } to="/register">REGISTER</Nav.Link>
						    </>
						) : user.isAdmin === true ? (
						    <>
						    	<Nav.Link className="dashboard" as={ NavLink } to="/dashboard">DASHBOARD</Nav.Link>
						    	<Nav.Link className="logout" as={ NavLink } to="/logout">LOG OUT</Nav.Link>
						    	<Navbar.Text className="mx-3">|</Navbar.Text>
						    	<Navbar.Text>Hi, {user.firstName}!</Navbar.Text>			      
						    </>
						) : (
						    <>
						    	<Nav.Link className="product" as={ NavLink } to="/products">PRODUCTS</Nav.Link>
						    	<Nav.Link className="logout" as={ NavLink } to="/logout">LOG OUT</Nav.Link>
						    	<Navbar.Text className="mx-3">|</Navbar.Text>
						    	<Navbar.Text>Hi, {user.firstName}!</Navbar.Text>
						    </>
						)}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}