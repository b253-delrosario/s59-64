import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { productId } = useParams();

	const { user } = useContext(UserContext);
	

	const navigate = useNavigate();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price , setPrice ] = useState(0);

	const buy = (productId) => {
		fetch(`http://localhost:4000/orders/checkout`,
		{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Checkout",
					icon: 'success',
					text: "You have successfully checked out this item."
				});

				navigate("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

	useEffect(() => {

		console.log(productId);

		fetch(`http://localhost:4000/products/${productId}`,
		{
			method: 'GET',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [ productId ]);
	

	return (

		<Container>
			<Row>
				<Col lg={{ span: 5, offset: 3}}>
					<Card className="my-4">
					    <Card.Body>
					        <Card.Title>{ name }</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{ description }</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php { price }</Card.Text>
					        { user.id !== null && user.isAdmin === false ? (
					        	<Button variant="primary" block onClick={() => buy(productId)}>Buy</Button>
					        )  : user.id !== null && user.isAdmin === true ? (
					        	<Link className="btn btn-danger btn-block" to="/dashboard">Back</Link>
					        )	:   (
					        	<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
					        )}
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)

}