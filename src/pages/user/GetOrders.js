import { useEffect, useState } from 'react';
import { Card, ListGroup, Container, Row, Col, Accordion } from 'react-bootstrap';

export default function GetAllOrders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch('http://localhost:4000/orders', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => setOrders(data))
      .catch((error) => console.error(error));
  }, []);

  return (
  	<Accordion className="mt-5">
  	  {orders.map((order) => (
  	    <Accordion.Item eventKey={order.user._id} key={order.user._id}>
  	      <Accordion.Header>
  	        {order.user.firstName} {order.user.lastName}
  	      </Accordion.Header>
  	      <Accordion.Body>
  	        <ListGroup variant="flush">
  	          {order.products.map((product) => (
  	            <ListGroup.Item key={product._id}>
  	              <h3>{product.name}</h3>
  	              <p>Price: &#x20B1;{product.price}</p>
  	              <p>Quantity: {product.quantity}</p>
  	              <p>Subtotal: &#x20B1;{product.subTotal}</p>
  	            </ListGroup.Item>
  	          ))}
  	          <ListGroup.Item>
  	            <p>Total: ${order.totalAmount}</p>
  	          </ListGroup.Item>
  	        </ListGroup>
  	      </Accordion.Body>
  	    </Accordion.Item>
  	  ))}
  	</Accordion>

  );
}





/*      
        <div >
          <h2>
            
          </h2>
          <ul>
            {order.products.map((product) => (
              <li }>
                <h3>{product.name}</h3>
                <p>Price: ${product.price}</p>
                <p>Quantity: </p>
                <p>Subtotal: $</p>
              </li>
            ))}
            <>
              <p></p>
              </>
          </ul>
        </div>
      ))}
    </div>
  );
}*/