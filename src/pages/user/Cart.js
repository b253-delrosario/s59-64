import { useEffect, useState } from 'react';
import { Card, ListGroup, Container, Row, Col, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import swal from 'sweetalert2';

export default function Cart() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch('http://localhost:4000/orders/user-orders', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          const modifiedData = data.map((order) => ({
            user: order.user,
            products: order.products.map((product) => ({
              productId: product.productId,
              name: product.name,
              description: product.description,
              price: product.price,
              quantity: product.quantity,
              subTotal: product.subTotal,
            })),
            totalAmount: order.totalAmount,
          }));
          setOrders(modifiedData);
        } else if (typeof data === 'object' && data !== null) {
          const modifiedData = {
            user: data.user,
            products: data.products.map((product) => ({
              productId: product.productId,
              name: product.name,
              description: product.description,
              price: product.price,
              quantity: product.quantity,
              subTotal: product.subTotal,
            })),
            totalAmount: data.totalAmount,
          };
          setOrders([modifiedData]);
        } else {
          console.error('Data is not an array or an object:', data);
        }
      })
      .catch((error) => console.error(error));
  }, []);

  const handleQuantityChange = (productId, newQuantity) => {
    const modifiedOrders = orders.map((order) => {
      const modifiedProducts = order.products.map((product) => {
        if (product.productId === productId) {
          return {
            ...product,
            quantity: newQuantity,
            subTotal: newQuantity * product.price,
          };
        }
        return product;
      });
      const modifiedTotalAmount = modifiedProducts.reduce(
        (total, product) => total + product.subTotal,
        0
      );
      return {
        ...order,
        products: modifiedProducts,
        totalAmount: modifiedTotalAmount,
      };
    });
    setOrders(modifiedOrders);
  };

  const handleCheckout = () => {
    // clear the orders state
    setOrders([]);
    swal('Checkout successful', 'Thank you for your purchase!', 'success');
  };

  return (
    <Container className="mt-5 justify-content-center">
      <Row>
        {orders.map((order) => (
          <Col md={4} key={order.user._id}>
            <Card.Header>
              {order.user.firstName} {order.user.lastName}
            </Card.Header>
            <Card.Body>
              {order.products.map((product) => (
                <Card className="mb-3" key={product.productId}>
                  <Card.Body>
                    <Card.Title>{product.name}</Card.Title>
                    <Card.Text>Price: &#x20B1;{product.price}</Card.Text>
                    <Card.Text>Qty: {product.quantity}</Card.Text>
                    <Card.Text>Subtotal: &#x20B1;{product.subTotal}</Card.Text>
                  </Card.Body>
                </Card>
              ))}
              <Card.Text>Total: ${order.totalAmount}</Card.Text>
              <Link
                to="/orders/checkout"
                className="btn btn-primary"
              >
                Checkout
              </Link>
            </Card.Body>
          </Col>
        ))}
      </Row>
    </Container>
  );
}
