import { useState } from 'react';
import Swal from 'sweetalert2';

export default function Checkout(props) {
  const [paymentMethod, setPaymentMethod] = useState('card');

  const handleSubmit = (e) => {
    e.preventDefault();
    // perform checkout logic here
    Swal.fire({
      icon: 'success',
      title: 'Order placed!',
      text: 'Your order has been successfully placed.',
    }).then(() => {
      // redirect user to home page or order confirmation page
    });
  };

  return (
    <div className="container my-5">
      <form onSubmit={handleSubmit}>
        {/* form fields here */}
        <button type="submit" className="btn btn-primary">
          Place Order
        </button>
      </form>
    </div>
  );
}
