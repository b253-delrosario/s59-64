import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate,  useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();


	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {
			// Prevents page redirection via form submission
			e.preventDefault();

			fetch(`http://localhost:4000/users/checkEmail`, {
			    method: "POST",
			    headers: {
			        'Content-Type': 'application/json'
			    },
			    body: JSON.stringify({
			        email: email
			    })
			})
			.then(res => res.json())
			.then(data => {

			    console.log(data);

			    if(data === true){

			    	Swal.fire({
			    		title: 'Sign Up Unsuccessful',
			    		icon: 'error',
			    		text: 'Email is already linked to an existing account. Try again using a different email.'	
			    	});

			    } else {

		            fetch(`http://localhost:4000/users/register`, {
		                method: "POST",
		                headers: {
		                    'Content-Type': 'application/json'
		                },
		                body: JSON.stringify({
		                    firstName: firstName,
		                    lastName: lastName,
		                    email: email,
		                    password: password1
		                })
		            })
		            .then(res => res.json())
		            .then(data => {

		                console.log(data);

		                if (data === true) {

		                    // Clear input fields
		                    setFirstName('');
		                    setLastName('');
		                    setEmail('');
		                    setPassword1('');
		                    setPassword2('');

		                    Swal.fire({
		                        title: 'Thank you for signing up',
		                        icon: 'success',
		                        text: 'Enjoy shopping!'
		                    });

		                    // Allows us to redirect the user to the login page after registering for an account
		                    navigate("/login");

		                } else {

		                    Swal.fire({
		                        title: 'Something wrong',
		                        icon: 'error',
		                        text: 'Please try again.'   
		                    });
		                }

					});
		        }
		    })
		}



	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if(
			firstName !=='' && 
			lastName !== '' && 
			email !== '' && 
			password1 !== '' && 
			password2 !== '' && 
			password1 === password2
		){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2]);

	return(

		(user.id !== null) ?
		    <Navigate to="/" />
		:
		<Container className="container-register">
			<Row>
				<Col lg={{ span: 4, offset: 4}} xs={{ span: 4, offset: 4}} md={{ span: 4, offset: 4}} className="text-center">
					<div className="text-center">
			    		<h2 className="mt-4">Create an Account</h2>
			    		<p id="reg-text">Sign up now to earn rewards and more!</p>
		    		</div>

					<Form onSubmit={(e) => registerUser(e)} className="mt-5 my-3">
				     	<Form.Group className="mb-3" controlId="firstName">
						  <Form.Control id="form-control-firstname"
						  	type="text" 
						  	placeholder="First Name"
						  	value={ firstName }
						  	onChange={e => setFirstName(e.target.value)}
						  	required/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="lastName">
						  <Form.Control id="form-control-lastname"
						  	type="text" 
						  	placeholder="Last Name"
						  	value={ lastName }
						  	onChange={e => setLastName(e.target.value)}
						  	required/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="userEmail">
						  <Form.Control id="form-control-email"
							type="email" 
							placeholder="Enter Email"
							value={ email }
							onChange={e => setEmail(e.target.value)}
							required/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="password1">
						  <Form.Control id="form-control-pw1"
							type="password" 
							placeholder="Password"
							value={ password1 }
							onChange={e => setPassword1(e.target.value)} 
							required/>
						  </Form.Group>

						<Form.Group className="mb-3" controlId="password2">
						  <Form.Control id="form-control-pw2"
							type="password" 
							placeholder="Verify Password"
							value={ password2 }
							onChange={e => setPassword2(e.target.value)}  
							required/>
						  </Form.Group>

						{ isActive ?
								<Button v type="submit" id="regBtn">
								  Sign Up
								</Button>
								:
								<Button  type="submit" id="regBtn-disabled" disabled>
								  Sign Up
								</Button>
							}
						</Form>
					<p id="login-reg-text">Already have an account? <Link id="login-reg" to="/login">Log in here!</Link></p>
				</Col>
			</Row>
		</Container>
		
	)
}