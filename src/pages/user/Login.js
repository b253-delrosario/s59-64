import { useState, useEffect, useContext } from 'react';

import { Form, Button, Col, Row, Container } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../../UserContext';

export default function Login() {

	// Allows us to consume the User context object and it's properties to use for user validation 
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

	function authenticate(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/login',
        {
        	method: 'POST',
        	headers: {
        		'Content-Type': 'application/json'
        	},
        	body: JSON.stringify({
        		email: email,
        		password: password
        	})
        })
        .then(res => res.json())
        .then(data => {

        	console.log(data);

        	if(typeof data.access !== "undefined"){

        		localStorage.setItem('token', data.access);
        		retrieveUserDetails(data.access);

        		console.log(data);

        		Swal.fire({
        			title: "Authentication Successful",
        			icon: "success",
        			html: "<p id='swal2-text'>You are now logged in.</p>",
        			allowOutsideClick: false,
			        allowEscapeKey: false
        		});
        	} else {

        		Swal.fire({
        			title: "Authentication Failed",
        			icon: "error",
        			html: "<p id='swal2-text'>The email or password you entered is incorrect. Please try again.</p>",
        			confirmButtonText: "Try again",
        			allowOutsideClick: false,
			        allowEscapeKey: false
        		});
        	}
        });

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {

    	fetch('http://localhost:4000/users/all', {
    		headers: {
    			Authorization: `Bearer ${ token }`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {

    		console.log(data);

    		setUser({
    			id: data._id,
    			isAdmin: data.isAdmin,
    			firstName: data.firstName
    		});
    	})
    };


	useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (

    	(user.id !== null) ?
    		<Navigate to="/"/>
    	:
    	<Container className="container-login">
    		<Row >
		    	<Col lg={{ span: 4, offset: 4}} xs={{ span: 4, offset: 4}} md={{ span: 4, offset: 4}} className="text-center">
		    		<div className="text-center">
			    		<h1 className="mt-4">Welcome Back!</h1>
			    		<p id="login-text">Log in to your account.</p>
		    		</div>
				    <Form onSubmit={e => authenticate(e)} className="mt-5 my-3">
				        <Form.Group className="mb-3" controlId="userEmail">
				            <Form.Control id="form-control-email"
				                type="email" 
				                placeholder="Email"
				                value={email}
				    			onChange={(e) => setEmail(e.target.value)}
				                required
				            />
				        </Form.Group>

				        <Form.Group className="mb-3" controlId="password">
				            <Form.Control id="form-control-pw"
				                type="password" 
				                placeholder="Password"
				                value={password}
				    			onChange={(e) => setPassword(e.target.value)}
				                required

				            />
				        </Form.Group>

				        { isActive ? 
				            <Button type="submit" id="loginBtn">
				                Log In
				            </Button>
				            : 
				            <Button type="submit" id="loginBtn-disabled" disabled>
				                Log In
				            </Button>
				        }
				    </Form>
				    	<p id="login-reg-text">Don't have an account? <Link id="login-reg" to="/register">Sign up here!</Link></p>
				</Col>
			</Row>
		</Container>
	)
}