import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useParams,  useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';

export default function UpdateProduct(){

	const { productId } = useParams();

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price , setPrice ] = useState(0);

	const [isActive, setIsActive] = useState(false);

	function updateOneProduct(e) {
			// Prevents page redirection via form submission
			e.preventDefault();

			fetch(`http://localhost:4000/products/${productId}/update`, 
			{
			    method: "PUT",
			    headers: {
			        'Content-Type': 'application/json',
			        "Authorization": `Bearer ${localStorage.getItem('token')}`
			    },
			    body: JSON.stringify({
			    	name: name,
			    	description: description,
			    	price: price
			    })
			})
			.then(res => res.json())
			.then(data => {

			    console.log(data);

			    if(data === true){

			    	Swal.fire({
			    		title: "Product Update Successful",
	        			icon: "success",
	        			html: "<p id='swal2-text'>Product details has been updated.</p>",
				        confirmButtonText: "Back to Dashboard",
				        cancelButtonText: "View product",
				        reverseButtons: true,
				        showCancelButton: true,
				        allowOutsideClick: false,
				        allowEscapeKey: false,
				        customClass: {
			        	    title: "swal2-title",
			        	    content: "swal2-text",
				            confirmButton: "swal2-confirm",
				            cancelButton: "swal2-cancel",
			        	}
			    	}).then((result) => {
			        if (result.isConfirmed) {
			          // Go back to dashboard
			          window.location.href = `/dashboard`;
			        } else if (result.dismiss === Swal.DismissReason.cancel) {
			          // View product
			          window.location.href = `/products/${productId}`;
			        }
					});

			    } else {

			    	Swal.fire({
			    		title: "Product Update Unsuccessful",
	        			icon: "error",
	        			html: "<p id='swal2-text'>Product details was not updated. Product may not be existing in the database.</p>",
				        confirmButtonText: "Back to Dashboard",
				        reverseButtons: true,
				        allowOutsideClick: false,
				        allowEscapeKey: false,
				        customClass: {
			        	    title: "swal2-title",
			        	    content: "swal2-text",
				            confirmButton: "swal2-confirm",
				            cancelButton: "swal2-cancel",
			        	}
			    	}).then((result) => {
			        if (result.isConfirmed) {
			          // Go back to dashboard
			          window.location.href = `/dashboard`;
			        } else {
			          // Update product / try again
			          window.location.href = "/";
			        }
					});
				}
			});
	}

	useEffect(() => {
	    fetch(`http://localhost:4000/products/${productId}`, {
	        method: "GET",
	        headers: {
	            'Content-Type': 'application/json',
	            "Authorization": `Bearer ${localStorage.getItem('token')}`
	        }
	    })
	    .then(res => res.json())
	    .then(data => {
	        setName(data.name);
	        setDescription(data.description);
	        setPrice(data.price);
	    })
	    .catch(error => {
	        console.log(error);
	    });
	}, [productId]);



	return(

		(user.isAdmin !== true) ?
    		navigate('/error', { replace: true })
    	:
		<Container className="container-login">
    		<Row >
		    	<Col lg={{ span: 4, offset: 4}} xs={{ span: 4, offset: 4}} md={{ span: 4, offset: 4}} className="text-center">
		    		<div className="text-center">
			    		<h1 className="mt-4">Update Product</h1>
		    		</div>
				    <Form onSubmit={e => updateOneProduct(e)} className="mt-5 my-3">
				        <Form.Group className="mb-3" controlId="productName">
				            <Form.Control id="form-control-name"
				                type="text" 
				                placeholder="Product Name"
				                value={name}
				    			onChange={(e) => setName(e.target.value)}
				                required
				            />
				        </Form.Group>

				        <Form.Group className="mb-3" controlId="description">
				            <Form.Control id="form-control-desc"
				                type="text" 
				                placeholder="Description"
				                value={description}
				    			onChange={(e) => setDescription(e.target.value)}
				                required
				            />
				        </Form.Group>

				         <Form.Group className="mb-3" controlId="price">
				            <Form.Control id="form-control-price"
				                type="number" 
				                placeholder="Price"
				                value={price}
				    			onChange={(e) => setPrice(e.target.value)}
				                required
				            />
				        </Form.Group>

				        { isActive ? 
				            <Button type="submit" id="prodBtn">
				                Update Product
				            </Button>
				            : 
				            <Button type="submit" id="prodBtn-disabled">
				                Update Product
				            </Button>
				        }
				    </Form>
				</Col>
			</Row>
		</Container>
	)
}