// ============ Admin Access - Dashboard ============ //

import { useEffect, useState } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { useNavigate, Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function GetAllProducts(){

    const [ product, setProducts ] = useState([]);
    const [ isActive, setIsActive ] = useState(null);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const navigate = useNavigate();

    const { productId } = useParams();
    const [reload, setReload] = useState(false);

    useEffect(() => {
        fetch(`http://localhost:4000/products/all`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            setProducts(data);
        });
    }, [reload]);

    product.sort((a, b) => {
      if (a.isActive && !b.isActive) return -1;
      if (!a.isActive && b.isActive) return 1;
      
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      
      return 0;
    });

    const archive = (productId) => {
      fetch(`http://localhost:4000/products/${productId}/archive`, 
      {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          isActive: false
        })
      })
        .then(res => res.json())
        .then(data => {

          console.log(data);

          if (data === true) {
          setReload(!reload); 
            Swal.fire({
              title: "Successful Product Archival",
              icon: 'success',
              html: "<p id='swal2-text'>Product has been archived and is now marked as inactive.</p>"
            });

          } else {

            Swal.fire({
              title: "Unsuccessful Product Archival",
              icon: "error",
              html: "<p id='swal2-text'>Product was already archived.</p>"
            });
          }
        })
      }

    const activate = (productId) => {
      fetch(`http://localhost:4000/products/${productId}/activate`, 
      {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          isActive: true
        })
      })
        .then(res => res.json())
        .then(data => {

          console.log(data);

          if (data === true) {
          setReload(!reload); 
            Swal.fire({
              title: "Successful Product Activation",
              icon: 'success',
              html: "<p id='swal2-text'>Product has been restored and is now marked as active.</p>"
            });

          } else {

            Swal.fire({
              title: "Unsuccessful Product Activation",
              icon: "error",
              html: "<p id='swal2-text'>Product is currently active.</p>"
            });
          }
        })
      }



    return (

        <>
          <div id="dashboardBtn">
            <Link className="btn btn-block mx-3" id="createBtn" to="/addNew">Create a Product</Link>
            <Link className="btn btn-block" id="checkOrderBtn">Check User Orders</Link>
          </div>

          <table className="table my-3">
              <thead id="tableHeader">
                  <tr>
                      <th style={{width: '20%'}}>Name</th>
                      <th style={{width: '50%'}}>Description</th>
                      <th style={{width: '10%'}}>Price</th>
                      <th style={{width: '10%'}}>Status</th>
                      <th style={{width: '10%'}}>Actions</th>
                  </tr>
              </thead>
              <tbody>
                  {product.map((product) => (
                      <tr key={product._id}>
                          <td id="column-product-name"><Link to={`/products/${product._id}`}>{product.name}</Link></td>
                          <td id="column-product-desc">{product.description}</td>
                          <td id="column-product-price">&#x20B1; {product.price}</td>
                          <td id="column-product-isActive">{product.isActive ? "Active" : "Inactive"}</td>
                          <td id="column-product-action">
                          <Link className="my-1 btn btn-block" id="updateBtn" to={`/products/${product._id}/update`}>Update</Link>
                          <br />
                          { product.isActive ? 
                          <Button className="my-1" id="archiveBtn" onClick={() => archive(product._id)}>Archive</Button>
                          : 
                          <Button className="my-1" id="activateBtn" onClick={() => activate(product._id)}>Activate</Button>
                          }
                          </td>
                      </tr>
                  ))}
              </tbody>
          </table>
        </>
    )
}
