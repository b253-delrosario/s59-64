import { useState, useEffect, useContext } from 'react';
import { Form, Button, Col, Row, Container } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../../UserContext';

export default function AddProduct() {

	// Allows us to consume the User context object and it's properties to use for user validation 
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

	function authenticate(e) {
        e.preventDefault();

        fetch('http://localhost:4000/products/create',
        {
        	method: 'POST',
        	headers: {
        		'Content-Type': 'application/json',
        		'Authorization': `Bearer ${localStorage.getItem('token')}`
        	},
        	body: JSON.stringify({
        		name: name,
        		description: description,
        		price: price
        	})
        })
        .then(res => res.json())
        .then(data => {

        	console.log(data);

        	if(data === true){

        		Swal.fire({
        			title: "Product Creation Successful",
        			icon: "success",
        			html: "<p id='swal2-text'>" + name + " is added to the list.</p>",
			        confirmButtonText: "Back to Dashboard",
			        cancelButtonText: "Create Another",
			        reverseButtons: true,
			        showCancelButton: true,
			        allowOutsideClick: false,
			        allowEscapeKey: false,
			        customClass: {
		        	    title: "swal2-title",
		        	    content: "swal2-text",
			            confirmButton: "swal2-confirm",
			            cancelButton: "swal2-cancel",
			        }
			      }).then((result) => {
			        if (result.isConfirmed) {
			          // Go back to dashboard
			          window.location.href = "/dashboard";
			        } else if (result.dismiss === Swal.DismissReason.cancel) {
			          // Create another product
			          setName('');
			          setDescription('');
			          setPrice('');
			        }
				});
        	} else {

        		Swal.fire({
        			title: "Product Creation Failed",
        			icon: "error",
        			html: "<p id='swal2-text'>Product (" + name + ") already exists but is currently archived.</p>",
        			confirmButtonText: "Go to Dashboard",
			        cancelButtonText: "Try again",
			        reverseButtons: true,
			        showCancelButton: true,
			        allowOutsideClick: false,
			        allowEscapeKey: false,
			        customClass: {
			        	title: "swal2-title",
			            confirmButton: "swal2-confirm",
			            cancelButton: "swal2-cancel"
			        }
        		}).then((result) => {
			        if (result.isConfirmed) {
			          // Go back to dashboard
			          window.location.href = "/dashboard";
			        } else if (result.dismiss === Swal.DismissReason.cancel) {
			          // Create another product
			          setName('');
			          setDescription('');
			          setPrice('');
			        }
				});
        	}
        });

        setName('');
        setDescription('');
        setPrice('');
    }


	useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(name !== '' && description !== '' && price !==''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [name, description, price]);

    return (

    	<Container className="container-login">
    		<Row >
		    	<Col lg={{ span: 4, offset: 4}} xs={{ span: 4, offset: 4}} md={{ span: 4, offset: 4}} className="text-center">
		    		<div className="text-center">
			    		<h1 className="mt-4">Create Product</h1>
		    		</div>
				    <Form onSubmit={e => authenticate(e)} className="mt-5 my-3">
				        <Form.Group className="mb-3" controlId="productName">
				            <Form.Control id="form-control-name"
				                type="text" 
				                placeholder="Product Name"
				                value={name}
				    			onChange={(e) => setName(e.target.value)}
				                required
				            />
				        </Form.Group>

				        <Form.Group className="mb-3" controlId="description">
				            <Form.Control id="form-control-desc"
				                type="text" 
				                placeholder="Description"
				                value={description}
				    			onChange={(e) => setDescription(e.target.value)}
				                required
				            />
				        </Form.Group>

				         <Form.Group className="mb-3" controlId="price">
				            <Form.Control id="form-control-price"
				                type="number" 
				                placeholder="Price"
				                value={price}
				    			onChange={(e) => setPrice(e.target.value)}
				                required
				            />
				        </Form.Group>

				        { isActive ? 
				            <Button type="submit" id="prodBtn">
				                Add Product
				            </Button>
				            : 
				            <Button type="submit" id="prodBtn-disabled" disabled>
				                Add Product
				            </Button>
				        }
				    </Form>
				</Col>
			</Row>
		</Container>
	)
}