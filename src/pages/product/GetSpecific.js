import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function ProductView() {

	const { productId } = useParams();

	const { user } = useContext(UserContext);
	

	const navigate = useNavigate();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price , setPrice ] = useState(0);
	const [ quantity , setQuantity ] = useState(0);
	const [isActive, setIsActive] = useState(null);

	const [reload, setReload] = useState(false);

	const buy = (productId) => {
		fetch(`http://localhost:4000/orders/add-to-cart`,
		{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			setQuantity(data.quantity);
			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Checkout",
					icon: 'success',
					text: "You have successfully checked out this item."
				});

				navigate("/products");

			} else {
				setReload(!reload); 
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

	useEffect(() => {

		console.log(productId);

		fetch(`http://localhost:4000/products/${productId}`,
		{
			method: 'GET',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setIsActive(data.isActive);
			setQuantity(0);
		})

	}, [ productId, reload]);

	console.log(name, description, price);
	

	return (

		(isActive === false && (user.isAdmin === false || user.id === null)) ?
		<Container className="d-flex justify-content-center" id="productView">
			<Card className="my-4" style={{ width: '40%' }}>
			    <Card.Body id="productCard">
			        <Card.Title>{ name }</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{ description }</Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>Php { price }</Card.Text>
			        { user.id !== null && user.isAdmin === false ? (
			        	<Button variant="primary" block onClick={() => buy(productId)} disabled>Buy</Button>
			        )  	:   (
			        	<Link className="btn btn-danger btn-block" to="/login" disabled>Log in to Buy</Link>
			        )}
			    </Card.Body>
			</Card>
		</Container>
		:
		<Container className="d-flex justify-content-center" id="productView">
			<Card className="my-4" style={{ width: '40%' }}>
			    <Card.Body>
			        <Card.Title>{ name }</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{ description }</Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>Php { price }</Card.Text>
			        { user.id !== null && user.isAdmin === false ? (
			        	<div>
                            <Form.Group className="mb-3" controlId="price">
					            <Form.Control id="form-control-price"
					                type="number" 
					                placeholder="Quantity"
					                value={quantity}
					    			onChange={(e) => setQuantity(e.target.value)}
					                required
					            />
					        </Form.Group>
                            <div id="productBtn">
                            <Button className="btn updateBtn btn-block mx-3" block onClick={() => buy(productId)} disabled={!quantity}>Add to Cart</Button>
                            <Link className="btn backBtn btn-block" to="/products">Back</Link>
                            </div>
                        </div>
			        )  : user.id !== null && user.isAdmin === true ? (
			        	<div id="productBtn">
				        	<Link className="btn updateBtn btn-block mx-3" to={`/products/${productId}/update`}>Update</Link>
				        	<Link className="btn backBtn btn-block" to="/dashboard">Back to Dashboard</Link>
			        	</div>
			        )	:   (
			        	<div id="productBtn">
			        	<Link className="btn updateBtn btn-block" to="/login">Log in to Buy</Link>
			        	</div>
			        )}
			    </Card.Body>
			</Card>
		</Container>
	)

}