import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, Container } from 'react-bootstrap';

export default function GetActiveProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/active`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  return (
    <Container className="text-center">
        <Row>
          {products.map((product) => (
            <Col xs={12} md={6} lg={3} key={product._id}>
              <Link className="btn" to={`/products/${product._id}`}>
                <Card id="product-card" className="p-4 mt-3 justify-content-center">
                  <Card.Body>
                    <Card.Title id="card-name-text">
                      {product.name}
                    </Card.Title>
                    {product.price && (
                      <Card.Text id="card-price-text">
                        &#x20B1; {product.price}
                      </Card.Text>
                    )}
                  </Card.Body>
                </Card>
              </Link>
            </Col>
          ))}
        </Row>
    </Container>
  );
}
