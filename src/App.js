import './App.css';

import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';

import { Route, Routes, BrowserRouter as Router } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';

// ========== OTHERS ========== //
import Error from './pages/Error';
import Home from './pages/Home';

// ========== USER ========== //
import Login from './pages/user/Login';
import Register from './pages/user/Register';
import Logout from './pages/user/Logout';
import GetOrders from './pages/user/GetOrders';
import Cart from './pages/user/Cart';
import Checkout from './pages/user/Checkout';

// ========== PRODUCT ========== //
import AddNew from './pages/product/AddNew';
import GetActive from './pages/product/GetActive';
import Dashboard from './pages/product/Dashboard';
import GetSpecific from './pages/product/GetSpecific';
import Update from './pages/product/Update';




// ========== ORDER ========== //



import { UserProvider } from './UserContext'

function App() {

  const [ user, setUser ] = useState({
      id: null,
      isAdmin: null,
      firstName: ""
  });


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (token) {
      fetch(`http://localhost:4000/users/details`,{
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          firstName: data.firstName
        })
      });
    }
  }, [])


  return (
  <UserProvider value={{ user, setUser, unsetUser }}>
    <Router>
      <AppNavbar/>
          <Container>     
            <Routes>
              <Route path="/AddNew" element={<AddNew />} />
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />}/>
              <Route path="/products" element={<GetActive />}/>
              <Route path="/orders" element={<GetOrders />}/>
              <Route path="/orders/cart" element={<Cart />}/>
              <Route path="/orders/checkout" element={<Checkout />}/>
              <Route path="/products/:productId" element={<GetSpecific />} />   
              <Route path="/products/:productId/update" element={<Update />} />   
              <Route path="/products/create" element={<AddNew />} />   
              <Route path="/register" element={<Register />} />
              <Route path="*" element={<Error />} />
            </Routes>
        </Container>
    </Router>
  </UserProvider>
  )
}

export default App;
